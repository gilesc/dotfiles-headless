#!/usr/bin/env bash

export PATH="$PATH":"$HOME/.local/bin"

if [ -f $HOME/.bashrc ]; then
    source $HOME/.bashrc
fi
