#!/usr/bin/env bash

if [ ! -d $HOME/dotfiles ]; then
    git clone git://bitbucket.org/gilesc/dotfiles-headless.git $HOME/.dotfiles
fi

ln -sf $HOME/.dotfiles/dotfilesrc $HOME/.dotfilesrc

if which dotfiles &> /dev/null; then
    dotfiles=dotfiles
else
    dotfiles=$HOME/.local/bin/dotfiles
    if [ ! -f $dotfiles ]; then
        which pip3 &> /dev/null || {
            echo "ERROR: Either the 'dotfiles' program or 'pip3' (Python 3's package manager) must be installed."
            echo "Aborting."
            exit 1
        }
        pip3 install --user dotfiles
    fi
fi

$dotfiles -sf
