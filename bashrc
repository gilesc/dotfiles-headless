# -*- mode: sh -*-

###################
# Utility functions
###################

source_if_exists() {
    if [ -e "$1" ]; then . "$1"; fi
}

# Only source if shell is interactive
if [[ $- != *i* ]] ; then
	return
fi

###############
# Shell options
###############

set -o vi

# Automatically resize window if terminal size changes
shopt -s checkwinsize
# If a directory is typed as a command, automatically prepend 'cd '
shopt -s autocd
# Share history among multiple bash shells
shopt -s histappend
# Fix minor spelling errors in cd commands
shopt -s cdspell
# Warn before exiting terminal if background jobs are running
shopt -s checkjobs
# Glob dotted files, case insensitive
shopt -s dotglob
shopt -s nocaseglob

########
# Prompt
########

source_if_exists /usr/share/git/git-prompt.sh
source_if_exists /usr/share/git/completion/git-completion.bash

BLACK='\[\e[1;30m\]'
RED='\[\e[1;31m\]'
GREEN='\[\e[1;32m\]'
YELLOW='\[\e[1;33m\]'
BLUE='\[\e[1;34m\]'
PURPLE='\[\e[1;35m\]'
CYAN='\[\e[1;36m\]'
WHITE='\[\e[1;37m\]'
COLOR_RESET='\[\033[00m\]'

if [ "$TERM" == "screen" ]; then
    PS1="$BLUE\w $GREEN\$ $COLOR_RESET"
else
    PS1="$GREEN\h $BLUE\w$PURPLE\$(__git_ps1)$GREEN \$ $COLOR_RESET"
fi

#########
# Aliases
#########

alias l='ls -oAhH --time-style=+ --color=auto'
alias ls='ls --color=auto'
alias la='ls -a --color=auto'
alias grep='grep --color=auto'

alias up='cd ..'
alias u='up'
alias uu='up && up'
alias uuu='uu && up'

alias g='git'
