==============
dotfiles-basic
==============

A simple set of dotfiles intended primarily for headless servers and optimized
for vim users.
